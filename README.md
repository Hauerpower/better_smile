# Szablon Bazowy HTML

Bazowy szablon do tworzenia stron oparty o Foundation, kompatybilny z Szablonem Bazowym WP

## Instalacja

Instalujemy wordpress na serwerze lokalnym

Po sklonowaniu repozytorium uruchamiamy
```shell
npm install
```

Aby rozpocząć pracę nad szablonem uruchamiamy komendę
```shell
gulp dev
```
W konsoli wyświetli się adres serwera, po wejściu i dokonaniu zmian w plikach przeglądarka odświeży się automatycznie

## Biblioteki
Szablon używa następujących bibliotek:
- slick-carousel
- fancybox - z dodanym wsparciem dla natywnych galerii WP
- lazysizes - z dodanym wsparciem do obrazków dodawanych w WP

### Dodawanie nowych bibliotek

Po zainstalowaniu bibliotek przez `npm install package_name` możemy dodać wymagane pliki .js do `JSlibs` w `gulpfile.js` - zostaną one dodane do `dist/libs.js`, który jest załadowany przez WP

Jeśli biblioteka wymaga plików css, dodajemy odpowiednią ścieżkę do `pluginsToCopy` w `gulpfile.js`.
Później dodajemy style w `<head>`

## Wgrywanie

### Commit
Po zakończeniu pracy uruchamiamy 
```shell
gulp prod
```
Szablon można wgrać na GitLab. Pages są skonfigurowane.
